﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace PhotoText
{
    class Program
    {
        static void Main(string[] args)
        {
            //перевод текста в буквы для обучения вроде
            //Bitmap pic = new Bitmap(@"E:\Саша\Фоточки\Мозайки\Фильтры\Чб\Тексты\зад 18 3.jpg");
            //DirToString("зад", "zad 1.txt", "обучение");

            //обучение
            /*Network nw = new Network("буквы");
            nw.Study("обучение");*/

            //распознавание текста
            //Bitmap pic = new Bitmap(@"зад 2.png");
            Bitmap pic = new Bitmap(@"тексты\хабр.png");
            System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
            timer.Interval = 5;
            timer.Start();
            timer.Tick += timer_Tick;
            string alphabet = "буквы - копия";
            RecognizeText rt = new RecognizeText(pic, 15, alphabet);
            rt.Recognize();

            //KNN
            //KNNTest();

            /*pic = rt.photo;
            pic.Save(@"зад\зад обрезанный.jpg");*/
            Console.WriteLine("я все!");
            Thread.Sleep(500000);
        }
        /// <summary>
        /// подгатавливает буквы для обучения. Берет директорию с буквами и текст, а по ним сохраняет изображения с соответствующими названиями.
        /// </summary>
        /// <param name="dir">директория с буквами</param>
        /// <param name="file">текст</param>
        /// <param name="save">директория, куда сохранять</param>
        public static void DirToString(string dir, string file, string save)
        {
            int Y = 0;
            StreamReader rdr = new StreamReader(file);
            string text = "";
            string line;
            text = rdr.ReadLine();
            while ((line = rdr.ReadLine()) != null)
                text += line;
            text = text.Replace(" ", "");
            text = text.Replace('?', '&');
            text = text.Replace(':', ';');
            text = text.ToLower();
            Console.WriteLine(text);
            Bitmap pic;
            string[] names = Directory.GetFiles(dir, "*.png");
            if (names.Length != text.Length)
            {
                Console.WriteLine("!!!" + (names.Length - text.Length).ToString());
                Console.WriteLine(text.Length.ToString());
                //Thread.Sleep(3000);
            }
	        for (int i = names.Length - 1; i >= 0; i--)
		        for (int j = 0; j < i; j++)
                    if (ToInt(names[j].Substring(dir.Length + 1, names[j].Length - 8)) > ToInt(names[j + 1].Substring(dir.Length + 1, names[j + 1].Length - 8)))
                    {
                        string s = names[j];
                        names[j] = names[j + 1];
                        names[j + 1] = s;
			        }
            int offset = 0;
            int prev = 0;
            int num = ToInt(names[0].Substring(dir.Length + 1, names[0].Length - 8));
            prev = num - 1;
            for (int i = 0; i < names.Length && names[i] != @"зад\156.png"; i++)
            {
                pic = new Bitmap(names[i]);
                num = ToInt(names[i].Substring(dir.Length + 1, names[i].Length - 8));
                offset += (num - (prev + 1));
                Console.WriteLine(offset);
                pic.Save(save + @"\" + text[num - offset] + " " + names[i].Substring(dir.Length + 1, names[i].Length - 8)/*Y.ToString()*/ + ".png");
                //pic.Save(save + @"\" + names[i].Substring(dir.Length + 1, names[i].Length - 8) + " " + text[num - offset] + ".png");
                Y++;
                prev = num;
            }
        }

        public static int ToInt(string s)
        {
            int n = 0, r = 1;
            for (int i = s.Length - 1; i >= 0 && (s[i] >= '0' && s[i] <= '9'); i--)
            {
                n += ((int)(s[i] - '0')) * r;
                r *= 10;
            }
            return n;
        }

        public static void KNNTest()
        {
            string learn = "буквы";
            string def = "обучение";
            string[] names = Directory.GetFiles(def);
            string s = "";
            KNN knn = new KNN(learn);
            for (int i = 0; i < names.Length; i++)
            {
                Console.WriteLine(GetChar(names[i]));
                s += knn.Measure(new Bitmap(names[i]));
            }
            Console.WriteLine(s);
        }

        public static char GetChar(string s)
        {
            for (int i = s.Length - 1; i >= 0; i--)
                if (s[i].ToString() == @"\")
                    return s[i + 1];
            Console.WriteLine("!");
            return s[0];
        }

        static void timer_Tick(object sender, EventArgs e)
        {
            GC.Collect();
        }
    }
}
