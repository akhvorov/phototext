﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace PhotoText
{
    class RecognizeText
    {
        public Bitmap photo;
        public List<Bitmap> Letters;
        public double coeffOfError;
        public Network network;
        public KNN knn;
        private string[] Dictionary;
        private Dictionary<string, int> dict;
        int Y = 0;
        //для скрина нормального текста - 10, для скана нормального - 30, для фотки - 50
        //private double error;

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="ph">фотка текста</param>
        /// <param name="coeff">коэффициент плохости изображения (скрин - 10, скана нормального - 20, фотки - 30)</param>
        public RecognizeText(Bitmap ph, int coeff, string dir)
        {
            photo = PreparePhoto(ph); //обработка фотографии
            //photo = ph;
            //photo.Save(@"текст 1 обр.png");
            Letters = new List<Bitmap>();
            coeffOfError = ((double)(coeff))/100;
            network = new Network(dir);
            knn = new KNN(dir);
            MakeDictionary();
            Y = 0;
        }
        #region Подготовка
        /// <summary>
        /// подготавливает фотографию к обработке
        /// </summary>
        /// <param name="main">фотография</param>
        /// <returns></returns>
        private Bitmap PreparePhoto(Bitmap main)
        {
            //return Median(MakeBW(main), 2);
            return MakeBW(main);
        }
        /// <summary>
        /// Сглаживает фотографию, убирая шумы
        /// </summary>
        /// <param name="main">изображение</param>
        /// <param name="n">3 или 5 - матрица свертки</param>
        /// <returns></returns>
        private Bitmap Smoothing(Bitmap main, int n)
        {
            int[,] matrix = new int[1, 1];
            matrix[0, 0] = 1;
            int len = 1, div = 1;
            switch (n)
            {
                case 3: matrix = new int[3, 3];
                    /*matrix[0, 0] = 2;
                    matrix[0, 1] = 3;
                    matrix[0, 2] = 2;
                    matrix[1, 0] = 3;
                    matrix[1, 1] = 5;
                    matrix[1, 2] = 3;
                    matrix[2, 0] = 2;
                    matrix[2, 1] = 3;
                    matrix[2, 2] = 2;
                    len = 3;
                    div = 25;*/
                    matrix[0, 0] = 1;
                    matrix[0, 1] = 2;
                    matrix[0, 2] = 1;
                    matrix[1, 0] = 2;
                    matrix[1, 1] = 4;
                    matrix[1, 2] = 2;
                    matrix[2, 0] = 1;
                    matrix[2, 1] = 2;
                    matrix[2, 2] = 1;
                    len = 3;
                    div = 16;
                    break;
                case 5:
                    matrix = new int[5, 5];
                    matrix[0, 0] = 1; matrix[0, 1] = 2; matrix[0, 2] = 3; matrix[0, 3] = 2; matrix[0, 4] = 1;
                    matrix[1, 0] = 2; matrix[1, 1] = 4; matrix[1, 2] = 6; matrix[1, 3] = 4; matrix[1, 4] = 2;
                    matrix[2, 0] = 3; matrix[2, 1] = 6; matrix[2, 2] = 10; matrix[2, 3] = 6; matrix[2, 4] = 3;
                    matrix[3, 0] = 2; matrix[3, 1] = 4; matrix[3, 2] = 6; matrix[3, 3] = 4; matrix[3, 4] = 2;
                    matrix[4, 0] = 1; matrix[4, 1] = 2; matrix[4, 2] = 3; matrix[4, 3] = 2; matrix[4, 4] = 1;
                    len = 5;
                    div = 82;
                    break;
                //case 10:
            }
            double r, g, b;
            Color col = new Color();
            Bitmap bmp = new Bitmap(main.Size.Width + len, main.Size.Height + len);
            for (int i = 0; i < main.Size.Width; i++)
            {
                for (int j = 0; j < main.Size.Height; j++)
                {
                    bmp.SetPixel(i + (len / 2), j + (len / 2), main.GetPixel(i, j));
                }
            }
            for (int width = (len / 2); width < main.Size.Width + (len / 2); width++)
            {
                for (int height = (len / 2); height < main.Size.Height + (len / 2); height++)
                {
                    r = 0;
                    g = 0;
                    b = 0;
                    for (int i = 0; i < len; i++)
                    {
                        for (int j = 0; j < len; j++)
                        {
                            col = bmp.GetPixel(width + i - (len / 2), height + j - (len / 2));
                            r += matrix[i, j] * col.R;
                            g += matrix[i, j] * col.G;
                            b += matrix[i, j] * col.B;
                        }
                    }
                    r = r / div;
                    g = g / div;
                    b = b / div;
                    col = Color.FromArgb((int)r, (int)g, (int)b);
                    bmp.SetPixel(width, height, col);
                }
            }
            for (int i = 0; i < main.Size.Width; i++)
            {
                for (int j = 0; j < main.Size.Height; j++)
                {
                    main.SetPixel(i, j, bmp.GetPixel(i + (len / 2), j + (len / 2)));
                }
            }
            //main.Save("smooth.png");
            return main;
        }
        /// <summary>
        /// делает изображение бинарно черно-белым
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        private Bitmap MakeBW(Bitmap main)
        {
            Color col = new Color();
            int k = 0;
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    k = (col.R + col.G + col.B) / 3;
                    col = Color.FromArgb(k, k, k);
                    main.SetPixel(width, height, col);
                }
            }
            int[][] mas = new int[main.Size.Width][];
            for (int i = 0; i < main.Size.Width; i++)
            {
                mas[i] = new int[main.Size.Height];
                for (int j = 0; j < main.Size.Height; j++)
                {
                    mas[i][j] = 0;
                }
            }
            int[][] counts = new int[main.Size.Width][];
            for (int i = 0; i < main.Size.Width; i++)
            {
                counts[i] = new int[main.Size.Height];
                for (int j = 0; j < main.Size.Height; j++)
                {
                    counts[i][j] = 0;
                }
            }
            int size = Math.Min(main.Size.Height, main.Size.Width);
            int av = 0;
            int count = 0;
            while (size > 5)
            {
                for (int width = 0; width < main.Size.Width; width += (size))
                {
                    for (int height = 0; height < main.Size.Height; height += (size))
                    {
                        for (int x = 0; (x < size) && (width + x < main.Size.Width); x++)
                        {
                            for (int y = 0; (y < size) && (height + y < main.Size.Height); y++)
                            {
                                col = main.GetPixel(width + x, height + y);
                                av += col.B;
                                mas[width + x][height + y] = (mas[width + x][height + y] * counts[width + x][height + y] + col.B) / (counts[width + x][height + y] + 1);
                                //counts[width + x][height + y]++;
                            }
                        }
                        av = av / (size * size);
                        for (int x = 0; (x < size) && (width + x < main.Size.Width); x++)
                        {
                            for (int y = 0; (y < size) && (height + y < main.Size.Height); y++)
                            {
                                mas[width + x][height + y] = (mas[width + x][height + y] * counts[width + x][height + y] + av) / (counts[width + x][height + y] + 1);
                                counts[width + x][height + y]++;
                            }
                        }
                    }
                }
                count++;
                size = (int)((double)size / 1.5);
            }
            k = 0;
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    if (col.B >= mas[width][height])
                    {
                        k = 255;
                    }
                    else
                    {
                        k = 0;
                    }
                    col = Color.FromArgb(k, k, k);
                    main.SetPixel(width, height, col);
                }
            }
            //main.Save("BW.png");
            return main;
        }
        /// <summary>
        /// Сглаживает изображение (Median)
        /// </summary>
        /// <param name="main"></param>
        /// <param name="n">сколько он рассматривает клеток и добавляет в список</param>
        /// <returns></returns>
        private Bitmap Median(Bitmap main, int n)
        {
            //double r, g, b;
            List<int> r = new List<int>();
            List<int> g = new List<int>();
            List<int> b = new List<int>();
            Color col = new Color();
            Bitmap bmp = new Bitmap(main.Size.Width + n, main.Size.Height + n);
            for (int i = 0; i < main.Size.Width; i++)
            {
                for (int j = 0; j < main.Size.Height; j++)
                {
                    bmp.SetPixel(i + (n / 2), j + (n / 2), main.GetPixel(i, j));
                }
            }
            for (int width = (n / 2); width < main.Size.Width + (n / 2); width++)
            {
                for (int height = (n / 2); height < main.Size.Height + (n / 2); height++)
                {
                    r.Clear();
                    g.Clear();
                    b.Clear();
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < n; j++)
                        {
                            col = bmp.GetPixel(width + i - (n / 2), height + j - (n / 2));
                            r.Add(col.R);
                            g.Add(col.G);
                            b.Add(col.B);
                        }
                    }
                    r.Sort();
                    g.Sort();
                    b.Sort();
                    /*r = Sort(r);
                    g = Sort(g);
                    b = Sort(b);*/
                    col = Color.FromArgb(r[n / 2], g[n / 2], b[n / 2]);
                    //col = Color.FromArgb(r[n - 1], g[n - 1], b[n - 1]);
                    //col = Color.FromArgb(r[0], g[0], b[0]);
                    main.SetPixel(width - (n / 2), height - (n / 2), col);
                }
            }
            //main.Save("median.png");
            return main;
        }
        /// <summary>
        /// инициализирует словарь
        /// </summary>
        /// <param name="dict">имя файла словаря</param>
        private void MakeDictionary(string dictFileName = "Словарь.txt"){
            StreamReader sr = new StreamReader(dictFileName, System.Text.Encoding.Default);
            string s = "", t = "";
            int i = 0, n = 0;
            List<string> list = new List<string>();
            dict = new Dictionary<string, int>();
            while(!sr.EndOfStream){
                n++;
                s = sr.ReadLine();
                i = s.Length - 1;
                while (s[i] != ' ')
                    i--;
                t = s.Substring(i + 1);
                list.Add(t);
                s = s.Substring(0, i);
                if(!dict.ContainsKey(t))
                    dict.Add(t, StrToInt(s));
                t = "";
            }
            sr.Close();
            Dictionary = list.ToArray();
        }
        private int StrToInt(string s)
        {
            int n = 0, r = 1;
            for (int i = s.Length - 1; i >= 0 && s[i] != ' '; i--)
            {
                n += (int)(s[i] - '0') * r;
                r *= 10;
            }
            return n;
        }
        #endregion
        public void Recognize()
        {
            PartitionText pt = new PartitionText(photo, coeffOfError);
            List<List<Bitmap>> words = pt.GetPartition();
            string text = "";
            string temp = "";
            for (int i = 0; i < words.Count; i++)
            {
                for (int j = 0; j < words[i].Count; j++)
                {
                    temp = GetWord(pt.GetWordPartition(i, j));
                    text += temp;
                    text += " ";
                    //Console.Write(temp + " ");
                }
                text += "\n";
                //Console.WriteLine();
            }
            Console.WriteLine();
            Console.WriteLine(text);
            Console.WriteLine("я все!");
        }
        /// <summary>
        /// Делает из приблизительного разбиения слова на картинки строковое слово
        /// </summary>
        /// <param name="pics">примерное разбиение слова</param>
        /// <returns>готовое слово</returns>
        public string GetWord(List<Bitmap> pics)
        {
            //обычный способ
            string s = GetFuzzyWord(pics);
            /*for (int i = 0; i < couples.Count; i++)
            {
                s += couples[i][0].ch;
            }*/
            return s;
            /*string s = "";
            for (int i = 0; i < couples.Count; i++)
            {
                s += couples[i][0].ch;
            }
            return FromDictionary(s);*/
        }
        /// <summary>
        /// выдает ближайшее слово из словаря
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        private string FromDictionary(string s)
        {
            string res = s;
            int min = s.Length;
            int n = 0;
            for (int i = 0; i < Dictionary.Length; i++)
            {
                n = DamLevMetric(s, Dictionary[i]);
                if (n < min)
                {
                    min = n;
                    res = Dictionary[i];
                }
            }
            //если расстояние больше трети блины слова (можно поменять) или частота встречания меньше, чем у первой половины популярных слов
            if (min > s.Length / 3 || dict[res] < dict[Dictionary[Dictionary.Length / 2]])
                return s;
            return res;
        }
        /// <summary>
        /// метрика Демерау-Левенштейна
        /// </summary>
        private int DamLevMetric(string one, string two)
        {
            int MAX = one.Length + two.Length;
            int[,] mas = new int[one.Length+1, two.Length + 1];
            for(int i = 0; i <= one.Length; i++)
                mas[i, 0] = i;
            for(int i = 0; i <= two.Length; i++)
                mas[0, i] = i;
            int n = 0;
            for(int i = 1; i <= one.Length; i++)
                for(int j = 1; j <= two.Length; j++){
                    n = mas[i - 1, j - 1] + Diff(one[i - 1], two[j - 1]);
                    mas[i, j] = Math.Min(Math.Min(mas[i - 1, j] + 1, mas[i, j - 1] + 1), n);
                }
            return mas[one.Length, two.Length];
        }
        /// <summary>
        /// расстояние между двумя буквами
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private int Diff(char a, char b)
        {
            if (a == b)
                return 0;
            else
                return 1;
        }

        #region Разбиение слова на буквы
        /// <summary>
        /// по предварительному разбиению слова на компоненты связности, 
        /// разбивает его уже наверняка и выдает списки вариантов букв, 
        /// подходящих на каждое место в слове
        /// </summary>
        /// <param name="pics">разбиение на компоненты связности</param>
        /// <returns>список списков пар (буква - кол-во встречаний)</returns>
        public string GetFuzzyWord(List<Bitmap> pics)
        {
            int num = 0;
            string s = "";
            /*for (int i = 0; i < pics.Count; i++)
            {
                pics[i].Save(@"зад\зад " + Y + " 2" + ".png");
                Y++;
            }*/
            for (int i = 0; i < pics.Count; i++)
            {
                num = NumOfParts(pics[i]);
                if (num <= 1)
                {
                    pics[i].Save(@"зад\" + Y.ToString() + ".png");
                    Y++;
                    //s += network.IdentifyLetter(pics[i]);
                    s += knn.Measure(pics[i]);
                    continue;
                }
                System.Drawing.Imaging.PixelFormat format = pics[i].PixelFormat;
                Rectangle rec = new Rectangle();
                int[] mas = Partition(pics[i], num); //не используется! TODO:написать нормальный Partition
                int a = mas[0], b;
                int w = pics[i].Size.Width / num;
                if (num > 1)
                    Console.WriteLine("Yeah: " + Y);
                for (int j = 0; j < num; j++)
                {
                    b = mas[j + 1];
                    rec = new Rectangle(w * j, 0, w, pics[i].Size.Height);
                    //rec = new Rectangle(a, 0, b - a, pics[i].Size.Height);
                    pics[i].Clone(rec, format).Save(@"зад\" + Y.ToString() + ".png");
                    Console.Write(Y + ": ");
                    Y++;
                    //s += network.IdentifyLetter(pics[i].Clone(rec, format));
                    s += knn.Nearest(pics[i].Clone(rec, format));
                    a = b;
                }
            }
            return s;
        }
        /// <summary>
        /// определяет на сколько букв делится компонента связности
        /// </summary>
        /// <param name="pic"></param>
        /// <returns></returns>
        private int NumOfParts(Bitmap pic)
        {
            int num = 0;
            int min = 0;
            int n = 0;
            int count1 = Math.Max(1, (pic.Size.Width / (/*2 * */pic.Size.Height))); //минимальное кол-во букв
            int count2 = (int)(3 * (double)pic.Size.Width / (double)pic.Size.Height); //максимальное число букв
            min = Sameness(pic, count1);
            num = count1;
            if (count2 <= 1)
                return 1;
            for (int j = count1; j <= count2; j++)
            {
                n = Sameness(pic, j);
                if (min > n)
                {
                    min = n;
                    num = j;
                }
            }
            return num;
        }
        /// <summary>
        /// показывает похожесть строки на слово (чем меньше, тем похожее)
        /// </summary>
        /// <param name="pic"></param>
        /// <param name="len">предполагаемая длина слова</param>
        /// <returns>похожесть</returns>
        public int Sameness(Bitmap pic, int len)
        {
            int sum = 0;
            System.Drawing.Imaging.PixelFormat format = pic.PixelFormat;
            Rectangle rec = new Rectangle();
            //int[] mas = Partition(pic, len);
            //int a = mas[0], b;
            int n = pic.Size.Width / len;
            //Console.WriteLine(n);
            for (int i = 0; i < len; i++)
            {
               // b = mas[i + 1];
                rec = new Rectangle(n*i, 0, n, pic.Size.Height);
                //rec = new Rectangle(a, 0, b - a, pic.Size.Height);
                //sum += network.MinDifference(pic.Clone(rec, format));
                sum += knn.MinDistance(pic.Clone(rec, format));
                //a = b;
            }
            return (sum / len);
        }
        /// <summary>
        /// написать нормальное разбиение!
        /// </summary>
        public int[] Partition(Bitmap pic, int len)
        {
            int l = 0, r = pic.Size.Width - 1;
            int n = 0;
            #region сужение границ
            while (n == 0)
            {
                for (int i = 0; i < pic.Size.Height; i++)
                    if (pic.GetPixel(l, i).B < 128)
                        n++;
                l++;
            }
            l--;
            n = 0;
            while (n == 0)
            {
                for (int i = 0; i < pic.Size.Height; i++)
                    if (pic.GetPixel(r, i).B < 128)
                        n++;
                r--;
            }
            r++;
            #endregion
            l++;
            r--;
            int[] mas = new int[len + 1];
            mas[0] = 0;
            mas[len] = pic.Size.Width - 1;
            List<int> list = SumOfHeights(pic);
            int MAX = 0;
            for (int i = 0; i < list.Count; i++)
                if (list[i] > MAX)
                    MAX = list[i];
            int min = MAX;
            int ind = 0;
            for (int i = 0; i < len - 1; i++)
            {
                for (int j = l; j < r; j++)
                    if (list[j] < min)
                    {
                        min = list[j];
                        ind = j;
                    }
                mas[i + 1] = ind;
                list[ind] = MAX + 1;
                min = MAX;
                ind = 0;
            }
            for (int i = 0; i < mas.Length; i++)
                Console.Write(mas[i] + " ");
            Console.WriteLine();
            return mas;
        }
        /// <summary>
        /// считает сумму черных пикселей в каждом столбике
        /// </summary>
        /// <param name="pic"></param>
        /// <returns></returns>
        public List<int> SumOfHeights(Bitmap pic)
        {
            List<int> nums = new List<int>();
            int sum = 0;
            int width = pic.Size.Width;
            int height = pic.Size.Height;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                    if (pic.GetPixel(i, j).B < 127)
                        sum++;
                nums.Add(sum);
                sum = 0;
            }
            return nums;
        }
        #endregion
    }
}
