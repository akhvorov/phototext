﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace PhotoText
{
    class PartitionText
    {
        public Bitmap photo;
        public double coeffOfError;
        List<List<Bitmap>> words;
        //List<List<PicRec>> pr;

        public PartitionText(Bitmap ph, double coeff)
        {
            photo = ph;
            coeffOfError = coeff;
            words = new List<List<Bitmap>>();
            //pr = new List<List<PicRec>>();
            photo = Cut(photo);
            //photo.Save(@"текст 1 обр.png");
            List<Bitmap> lines = GetLines();
            //List<Rectangle> prlines = GetLines();
            List<Bitmap> letters = new List<Bitmap>();
            for (int i = 0; i < lines.Count; i++)
            {
                words.Add(GetWords(lines[i]));
                //lines[i].Save(@"зад\зад " + i.ToString() + ".png");
                lines[i].Dispose();
                /*for (int j = 0; j < words[i].Count; j++)
                {
                    words[i][j].Save(@"зад\зад " + i.ToString() + " " + j.ToString() + ".png");
                    letters = GetLetters(words[i][j]);
                    for (int k = 0; k < letters.Count; k++)
                    {
                        letters[k].Save(@"зад\зад " + i.ToString() + " " + j.ToString() + " " + k.ToString() + ".png");
                    }
                }*/
            }
        }

        public List<List<Bitmap>> GetPartition()
        {
            return words;
        }

        public List<Bitmap> GetWordPartition(int line, int word)
        {
            return GetLetters(words[line][word]);
        }
        /// <summary>
        /// обрезает изображение
        /// </summary>
        /// <param name="pic">изображение</param>
        /// <param name="all">обрезать без краев</param>
        /// <returns></returns>
        public PicRec CutPR(PicRec pic, bool all = false)
        {
            double error = 0.1 * coeffOfError;
            int p = 2; //в запасе оставить по краям
            int sum = 0; //количество белых пикселей
            int x = 0, y = 0, width = pic.pic.Size.Width, height = pic.pic.Size.Height;
            #region четыре цикла
            for (int i = 0; i < pic.pic.Size.Width; i++)
            {
                for (int j = 0; j < pic.pic.Size.Height; j++)
                {
                    if (pic.pic.GetPixel(i, j).B > 127)
                        sum++;
                }
                if (((double)(pic.pic.Size.Height - sum)) / ((double)pic.pic.Size.Height) < error)
                {
                    x++;
                }
                else
                {
                    break;
                }
                sum = 0;
            }
            /*if (x - p >= 0)
            {
                x -= p;
            }*/
            if(!all) x -= Math.Min(p, x);
            for (int i = pic.pic.Size.Width - 1; i >= 0; i--)
            {
                for (int j = 0; j < pic.pic.Size.Height; j++)
                {
                    if (pic.pic.GetPixel(i, j).B > 127)
                        sum++;
                }
                if (((double)(pic.pic.Size.Height - sum)) / ((double)pic.pic.Size.Height) < error)
                {
                    width--;
                }
                else
                {
                    break;
                }
                sum = 0;
            }
            /*if (width + p < pic.Size.Width)
            {
                width += p;
            }*/
            if (!all) width += Math.Min(p, pic.pic.Size.Width - width);
            for (int i = 0; i < pic.pic.Size.Height; i++)
            {
                for (int j = 0; j < pic.pic.Size.Width; j++)
                {
                    if (pic.pic.GetPixel(j, i).B > 127)
                        sum++;
                }
                if (((double)(pic.pic.Size.Width - sum)) / ((double)pic.pic.Size.Width) < error)
                {
                    y++;
                }
                else
                {
                    break;
                }
                sum = 0;
            }
            /*if (y - p >= 0)
            {
                y -= p;
            }*/
            if (!all) y -= Math.Min(p, y);
            for (int i = pic.pic.Size.Height - 1; i >= 0; i--)
            {
                for (int j = 0; j < pic.pic.Size.Width; j++)
                {
                    if (pic.pic.GetPixel(j, i).B > 127)
                        sum++;
                }
                if (((double)(pic.pic.Size.Width - sum)) / ((double)pic.pic.Size.Width) < error)
                {
                    height--;
                }
                else
                {
                    break;
                }
                sum = 0;
            }
            /*if (height + p < pic.Size.Height)
            {
                height += p;
            }*/
            if (!all) height += Math.Min(p, pic.pic.Size.Height - height);
            #endregion
            Rectangle rec = new Rectangle(new Point(x, y), new Size(width - x, height - y));
            //return rec;
            //System.Drawing.Imaging.PixelFormat format = pic.PixelFormat;
            GC.Collect();
            if (width < x || height < y)
                return pic;
            Bitmap bmp = new Bitmap(width - x, height - y);
            for (int i = 0; i < bmp.Size.Width; i++)
                for (int j = 0; j < bmp.Size.Height; j++)                
                    bmp.SetPixel(i, j, pic.pic.GetPixel(i + x, j + y));
            pic.pic = bmp;
            pic.rec = rec;
            GC.Collect();
            return pic;
            //return pic.Clone(rec, format);
        }
        /// <summary>
        /// обрезает изображение
        /// </summary>
        /// <param name="pic">изображение</param>
        /// <param name="all">обрезать без краев</param>
        /// <returns></returns>
        public Bitmap Cut(Bitmap pic, bool all = false)
        {
            double error = 0.1 * coeffOfError;
            int p = 2; //в запасе оставить по краям
            int sum = 0; //количество белых пикселей
            int x = 0, y = 0, width = pic.Size.Width, height = pic.Size.Height;
            #region четыре цикла
            for (int i = 0; i < pic.Size.Width; i++)
            {
                for (int j = 0; j < pic.Size.Height; j++)
                {
                    if (pic.GetPixel(i, j).B > 127)
                        sum++;
                }
                if (((double)(pic.Size.Height - sum)) / ((double)pic.Size.Height) < error)
                {
                    x++;
                }
                else
                {
                    break;
                }
                sum = 0;
            }
            /*if (x - p >= 0)
            {
                x -= p;
            }*/
            if (!all) x -= Math.Min(p, x);
            for (int i = pic.Size.Width - 1; i >= 0; i--)
            {
                for (int j = 0; j < pic.Size.Height; j++)
                {
                    if (pic.GetPixel(i, j).B > 127)
                        sum++;
                }
                if (((double)(pic.Size.Height - sum)) / ((double)pic.Size.Height) < error)
                {
                    width--;
                }
                else
                {
                    break;
                }
                sum = 0;
            }
            /*if (width + p < pic.Size.Width)
            {
                width += p;
            }*/
            if (!all) width += Math.Min(p, pic.Size.Width - width);
            for (int i = 0; i < pic.Size.Height; i++)
            {
                for (int j = 0; j < pic.Size.Width; j++)
                {
                    if (pic.GetPixel(j, i).B > 127)
                        sum++;
                }
                if (((double)(pic.Size.Width - sum)) / ((double)pic.Size.Width) < error)
                {
                    y++;
                }
                else
                {
                    break;
                }
                sum = 0;
            }
            /*if (y - p >= 0)
            {
                y -= p;
            }*/
            if (!all) y -= Math.Min(p, y);
            for (int i = pic.Size.Height - 1; i >= 0; i--)
            {
                for (int j = 0; j < pic.Size.Width; j++)
                {
                    if (pic.GetPixel(j, i).B > 127)
                        sum++;
                }
                if (((double)(pic.Size.Width - sum)) / ((double)pic.Size.Width) < error)
                {
                    height--;
                }
                else
                {
                    break;
                }
                sum = 0;
            }
            /*if (height + p < pic.Size.Height)
            {
                height += p;
            }*/
            if (!all) height += Math.Min(p, pic.Size.Height - height);
            #endregion
            Rectangle rec = new Rectangle(new Point(x, y), new Size(width - x, height - y));
            //return rec;
            //System.Drawing.Imaging.PixelFormat format = pic.PixelFormat;
            GC.Collect();
            if (width < x || height < y)
                return pic;
            Bitmap bmp = new Bitmap(width - x, height - y);
            for (int i = 0; i < bmp.Size.Width; i++)
                for (int j = 0; j < bmp.Size.Height; j++)
                    bmp.SetPixel(i, j, pic.GetPixel(i + x, j + y));
            GC.Collect();
            return bmp;
            //return pic.Clone(rec, format);
        }
        /// <summary>
        /// должен выравнивать, потом сделать
        /// </summary>
        public void Align()
        {

        }
        /// <summary>
        /// получает вырезанные изобраения строк
        /// </summary>
        /// <returns></returns>
        public List<PicRec> GetLinesPR()
        {
            //List<Bitmap> lines = new List<Bitmap>();
            List<PicRec> recs = new List<PicRec>();
            Rectangle rec;
            System.Drawing.Imaging.PixelFormat format = photo.PixelFormat;
            List<int> nums = WhitnessOfLines();
            int a = nums[0], b;
            GC.Collect();
            //Bitmap bmp;
            for (int i = 1; i < nums.Count; i++)
            {
                b = nums[i];
                rec = new Rectangle(0, a, photo.Size.Width, b - a);
                GC.Collect();
                //Cut(photo.Clone(rec, format)).Save(@"зад\зад " + i.ToString() + ".png"); //можно сохранять
                //bmp = photo.Clone(rec, format);
                recs.Add(new PicRec(photo.Clone(rec, format), rec));
                //lines.Add(photo.Clone(rec, format));
                //lines.Add(photo.Clone(rec, format));
                GC.Collect();
                a = b;
            }
            //TODO:убирать строки, где очень мало черного (ошибки, пустые строки)
            for (int i = recs.Count - 1; i >= 0; i--)
                if (IsEmpty(recs[i].pic))
                    recs.RemoveAt(i);
            for (int i = recs.Count - 1; i >= 0; i--)
                recs[i] = CutPR(recs[i]);
            return recs;
        }

        /// <summary>
        /// получает вырезанные изобраения строк
        /// </summary>
        /// <returns></returns>
        public List<Bitmap> GetLines()
        {
            List<Bitmap> lines = new List<Bitmap>();
            Rectangle rec;
            System.Drawing.Imaging.PixelFormat format = photo.PixelFormat;
            List<int> nums = WhitnessOfLines();
            int a = nums[0], b;
            GC.Collect();
            //Bitmap bmp;
            for (int i = 1; i < nums.Count; i++)
            {
                b = nums[i];
                rec = new Rectangle(0, a, photo.Size.Width, b - a);
                GC.Collect();
                //Cut(photo.Clone(rec, format)).Save(@"зад\зад " + i.ToString() + ".png"); //можно сохранять
                //bmp = photo.Clone(rec, format);
                lines.Add(photo.Clone(rec, format));
                //lines.Add(photo.Clone(rec, format));
                GC.Collect();
                a = b;
            }
            //TODO:убирать строки, где очень мало черного (ошибки, пустые строки)
            for (int i = lines.Count - 1; i >= 0; i--)
                if (IsEmpty(lines[i]))
                    lines.RemoveAt(i);
            for (int i = lines.Count - 1; i >= 0; i--)
                lines[i] = Cut(lines[i]);
            return lines;
        }
        /// <summary>
        /// получает список с номерами, по которым делать разрез на строки
        /// </summary>
        /// <returns></returns>
        public List<int> WhitnessOfLines()
        {
            List<int> list = new List<int>();
            int sum = 0;//кол-во черных
            int height = photo.Size.Height;
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < photo.Size.Width; j++)
                    if (photo.GetPixel(j, i).B < 127)
                        sum++;
                list.Add(sum);
                sum = 0;
            }
            GC.Collect();
            return ChooseLines(list);
        }
        /// <summary>
        /// выбирает, где поставить разграничители линий
        /// </summary>
        /// <param name="nums">список количеств черных для каждой строки</param>
        /// <returns></returns>
        public List<int> ChooseLines(List<int> nums)
        {
            double error = 0.05 * coeffOfError;
            int width = photo.Size.Width;
            int diff = 8; //минимальная разница между строк
            List<int> lines = new List<int>(); //выходной список с нужными номерами
            int ind = 0;
            int min = width;
            int count = 0;
            for (int i = nums.Count - 1; i >= 0; i--)
            {
                while (i >= 0 && ((double)nums[i]) / ((double)width) < error)
                {
                    if (min > nums[i])
                    {
                        min = nums[i];
                        ind = i;
                    }
                    count++;
                    i--;
                }
                min = width;
                if (count > 0)
                {
                    lines.Add(ind);
                    if (i > ind - diff)
                        i = ind - diff;
                    count = 0;
                }
            }
            if (lines[lines.Count - 1] > 4)
                lines.Add(0);
            //убрать одинаковые
            count = lines.Count;
            for (int i = count - 2; i >= 0; i--)
                if (lines[i] == lines[i + 1])
                    lines.RemoveAt(i + 1);
            //перевернуть
            int len = lines.Count;
            for (int i = 0; i < len / 2; i++)
            {
                int temp = lines[i];
                lines[i] = lines[len - i - 1];
                lines[len - i - 1] = temp;
            }
            if (lines[lines.Count - 1] < photo.Size.Height - 4)
                lines.Add(photo.Size.Height - 1);
            GC.Collect();
            return lines;
        }
        //TODO:рассматривать области на случай, если там очень короткое слово
        /// <summary>
        /// говорит, является ли это изображение пустым настолько, чтобы считать его ошибочным и не содержащем текст
        /// </summary>
        /// <param name="pic"></param>
        /// <returns></returns>
        public bool IsEmpty(Bitmap pic)
        {
            int sum = 0;
            for (int i = 0; i < pic.Size.Width; i++)
                for (int j = 0; j < pic.Size.Height; j++)
                    if (pic.GetPixel(i, j).B < 127)
                        sum++;
            double percent = 0.05;
            return (sum < pic.Size.Width * pic.Size.Height * percent);
        }
        /// <summary>
        /// дают строчку, возвращает список слов
        /// </summary>
        /// <param name="pic">строчка</param>
        /// <returns>список слов</returns>
        public List<Bitmap> GetWords(Bitmap pic)
        {
            Rectangle rec;
            System.Drawing.Imaging.PixelFormat format = pic.PixelFormat;
            List<Bitmap> words = new List<Bitmap>();
            List<int> sums = new List<int>();
            int sum = 0;
            int width = pic.Size.Width;
            int height = pic.Size.Height;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                    if (pic.GetPixel(i, j).B < 127)
                        sum++;
                sums.Add(sum);
                sum = 0;
            }
            List<int> nums = ChooseWords(sums, pic.Size.Height, 5);
            int a = nums[0], b;
            GC.Collect();
            for (int i = 1; i < nums.Count; i++)
            {
                b = nums[i];
                //Console.WriteLine("rec: " + a + ", " + 0 + ", " + (b - a) + ", " + height);
                //rec = new Rectangle(a, 0, b - a, height);
                //Cut(photo.Clone(rec, format)).Save(@"зад\зад " + i.ToString() + ".png"); //можно сохранять
                words.Add(Clone(pic, a, 0, b - a, height));
                //words.Add(Clone(pic, rec));
                GC.Collect();
                a = b;
            }
            for (int i = words.Count - 1; i >= 0; i--)
                if (IsEmpty(words[i]))
                    words.RemoveAt(i);
            for (int i = words.Count - 1; i >= 0; i--)
                words[i] = Cut(words[i]);
            return words;
        }
        /// <summary>
        /// выбирает, где провести гораницы слов
        /// </summary>
        /// <param name="nums">список сумм черных точек по всем полосам</param>
        /// <param name="height">высота</param>
        /// <param name="row">какое минимальное расстояние между буквами (для слов - 2, для букв - 1?)</param>
        /// <returns>список, по которому резать</returns>
        public List<int> ChooseWords(List<int> nums, int height, int row)
        {
            //TODO:придумать, что сделать с границей изменяющейся в зависимсти от коэффициента и высоты строки
            //row = row * height / 6;
            row *= 1;
            List<int> lines = new List<int>(); //выходной список с нужными номерами
            int count = 0;
            for (int i = nums.Count - 1; i >= 0; i--)
            {
                //while (i >= 0 && ((double)nums[i]) / ((double)height) < error)
                while (i >= 0 && nums[i] == 0)
                {
                    count++;
                    i--;
                }
                //i++;
                //min = height;
                if (count >= row)
                {
                    lines.Add(i + (count/*/2*/)); //чтобы резать не по границе буквы, а с запасом, посередине
                    /*if (i > ind - diff)
                        i = ind - diff;*/
                }
                //i--;
                count = 0;
            }
            if (lines.Count > 1 && lines[lines.Count - 1] > 4)
                lines.Add(0);
            int len = lines.Count;
            for (int i = 0; i < len / 2; i++)
            {
                int temp = lines[i];
                lines[i] = lines[len - i - 1];
                lines[len - i - 1] = temp;
            }
            if (lines.Count > 1 && lines[lines.Count - 1] < nums.Count - 4)
                lines.Add(nums.Count - 1);
            return lines;
        }
        /// <summary>
        /// выдает примерный список букв
        /// </summary>
        public List<Bitmap> GetLetters(Bitmap pic)
        {
            List<Bitmap> letters = new List<Bitmap>();
            List<int> sums = new List<int>();
            int sum = 0;
            int width = pic.Size.Width;
            int height = pic.Size.Height;
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                    if (pic.GetPixel(i, j).B < 127)
                        sum++;
                sums.Add(sum);
                sum = 0;
            }
            List<int> nums = ChooseWords(sums, pic.Size.Height, 1);
            if (nums.Count <= 1)
            {
                letters.Add(pic);
                return letters;
            }
            int a = nums[0], b;
            GC.Collect();
            for (int i = 1; i < nums.Count; i++)
            {
                b = nums[i];
                letters.Add(Clone(pic, a, 0, b - a, height));
                GC.Collect();
                a = b;
            }
            for (int i = letters.Count - 1; i >= 0; i--)
                if (IsEmpty(letters[i]))
                    letters.RemoveAt(i);
            /*for (int i = letters.Count - 1; i >= 0; i--)
            {
                letters[i] = Cut(letters[i]); //!!!добавить, чтоб буквы были одинаковыми
            }*/
            return letters;
        }

        /// <summary>
        /// аналог Bitmap.Clone
        /// </summary>
        public Bitmap Clone(Bitmap pic, int x, int y, int width, int height)
        {
            Bitmap bmp = new Bitmap(width, height);
            for (int i = 0; i < bmp.Size.Width; i++)
                for (int j = 0; j < bmp.Size.Height; j++)
                    bmp.SetPixel(i, j, pic.GetPixel(i + x, j + y));
            GC.Collect();
            return bmp;
        }
        /// <summary>
        /// аналог Bitmap.Clone
        /// </summary>
        public Bitmap Clone(Bitmap pic, Rectangle rec)
        {
            Bitmap bmp = new Bitmap(rec.Width, rec.Height);
            for (int i = 0; i < bmp.Size.Width; i++)
                for (int j = 0; j < bmp.Size.Height; j++)
                    bmp.SetPixel(i, j, pic.GetPixel(i + rec.X, j + rec.Y));
            GC.Collect();
            return bmp;
        }
    }

    public class PicRec
    {
        public Bitmap pic;
        public Rectangle rec;

        public PicRec(Bitmap p, Rectangle r)
        {
            pic = p;
            rec = r;
        }
    }
}
