﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace PhotoText
{
    class KNN
    {
        Letter[] Letters;
        int n; //размер изображений
        int k; //сколько выбирать будем

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="learn">директория, где мы знаем все</param>
        public KNN(string learn)
        {
            n = 25;
            k = 15;
            Prepre(learn);
        }

        private void Prepre(string learn)
        {
            string[] names = Directory.GetFiles(learn);
            Letters = new Letter[names.Length];
            char ch;
            for(int i = 0; i < names.Length; i++)
                Letters[i] = new Letter(new Bitmap(names[i]), GetChar(names[i]), n);
        }

        /// <summary>
        /// определяет, что за буква по заданной картинке
        /// </summary>
        /// <param name="pic">картинка</param>
        /// <returns>символ, которому соответствует</returns>
        public char Measure(Bitmap pic)
        {
            int[] v = GetVector(pic);
            Couple[] lens = new Couple[Letters.Length];
            for (int i = 0; i < lens.Length; i++)
                lens[i] = new Couple(Letters[i].ch, Distance(v, Letters[i].vector));
            Heap<Couple> heap = new Heap<Couple>(lens);
            Couple[] nearest = heap.GetKMin(k);
            Dictionary<char, int> map = new Dictionary<char, int>();
            foreach (Couple a in nearest)
                if (map.ContainsKey(a.ch))
                    map[a.ch]++;
                else
                    map.Add(a.ch, 1);
            //для отсортированного
            heap.Clear();
            List<Couple> list = new List<Couple>();
            foreach (KeyValuePair<char, int> a in map) //добавляем в кучу
                heap.Add(new Couple(a.Key, a.Value));
            for (int i = 0; i < map.Count; i++) //добавляем в список
                list.Add(heap.GetMin());
            list.Reverse(); //переворачивает список
            //return list; //вернуть множество возможных
            //для максимума
            Couple max = new Couple('a', 0);
            foreach (KeyValuePair<char, int> a in map)
                if (a.Value > max.num)
                {
                    max.num = a.Value;
                    max.ch = a.Key;
                }
            return max.ch;
        }
        /// <summary>
        /// выдает все ближайшие точки, отсортированные по убыванию вероятности быть верным ответом
        /// </summary>
        /// <param name="pic"></param>
        /// <returns></returns>
        public List<Couple> Nearest(Bitmap pic)
        {
            int[] v = GetVector(pic);
            Couple[] lens = new Couple[Letters.Length];
            for (int i = 0; i < lens.Length; i++)
                lens[i] = new Couple(Letters[i].ch, Distance(v, Letters[i].vector));
            Heap<Couple> heap = new Heap<Couple>(lens);
            Couple[] nearest = heap.GetKMin(k);
            Dictionary<char, int> map = new Dictionary<char, int>();
            foreach (Couple a in nearest)
                if (map.ContainsKey(a.ch))
                    map[a.ch]++;
                else
                    map.Add(a.ch, 1);
            //для отсортированного
            heap.Clear();
            List<Couple> list = new List<Couple>();
            foreach (KeyValuePair<char, int> a in map) //добавляем в кучу
                heap.Add(new Couple(a.Key, a.Value));
            for (int i = 0; i < map.Count; i++) //добавляем в список
                list.Add(heap.GetMin());
            list.Reverse(); //переворачивает список
            return list; //вернуть множество возможных
        }

        public int MinDistance(Bitmap pic)
        {
            int[] v = GetVector(pic);
            Couple[] lens = new Couple[Letters.Length];
            for (int i = 0; i < lens.Length; i++)
                lens[i] = new Couple(Letters[i].ch, Distance(v, Letters[i].vector));
            Heap<Couple> heap = new Heap<Couple>(lens);
            Couple[] nearest = heap.GetKMin(k);
            Couple max = new Couple('a', n*n*256);
            foreach (Couple a in nearest)
                if (max.num > a.num)
                {
                    max.num = a.num;
                    max.ch = a.ch;
                }
            return max.num;
        }
        /// <summary>
        /// вычисляет расстояние между точками с заданными координатами
        /// </summary>
        private int Distance(int[] v, int[] u)
        {
            int d = 0;
            int h = 0;
            for (int i = 0; i < v.Length; i++)
            {
                h = v[i] - u[i];
                d += h * h;
            }
            return d;
        }
        /// <summary>
        /// дает вектор подаваемого изображения
        /// </summary>
        private int[] GetVector(Bitmap pic)
        {
            pic = new Bitmap(pic, new Size(n, n));
            int[] v = new int[n * n];
            int k = 0;
            Color col;
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                {
                    col = pic.GetPixel(i, j);
                    v[k] = (int)((col.R + col.G + col.B) / 3);
                    k++;
                }
            return v;
        }

        private char GetChar(string s)
        {
            for(int i = s.Length - 1; i >= 0; i--)
                if(s[i].ToString() == @"\")
                    return s[i + 1];
            Console.WriteLine("!");
            return s[0];
        }
    }

    class Letter{
        public int[] vector;
        public char ch;

        public Letter(Bitmap pic, char c, int n){
            ch = c;
            pic = new Bitmap(pic, new Size(n, n));
            vector = new int[n * n];
            int k = 0;
            Color col;
            for (int i = 0; i < n; i++)
                for (int j = 0; j < n; j++)
                {
                    col = pic.GetPixel(i, j);
                    vector[k] = (int)((col.B + col.G + col.R) / 3);
                    k++;
                }
            pic.Dispose();
        }
    }

    class Couple : IComparable<Couple>
    {
        public char ch;
        public int num;

        public Couple(char c, int n)
        {
            ch = c;
            num = n;
        }

        public /*override*/ int CompareTo(Couple c)
        {
            return num - c.num;
        }
    }

    class Heap<T> where T : IComparable<T>
    {
        T[] m;
        int n;

        public Heap()
        {
            n = 0;
            m = new T[15];
        }

        public Heap(int k)
        {
            n = 0;
            m = new T[k];
        }

        public Heap(T[] mas)
        {
            m = mas;
            n = mas.Length;
            for (int i = n / 2; i >= 0; i--)
                Heapify(i);
        }

        public void Add(T a)
        {
            if (n == m.Length)
                Increase();
            m[n] = a;
            int i = n;
            int r = (n - 1) / 2;
            while (r >= 0 && m[i].CompareTo(m[r]) < 0) //для min
            {
                swap(i, r);
                i = r;
                r = (r - 1) / 2;
            }
            n++;
        }

        private void Increase()
        {
            int l = 2 * n;
            T[] arr = new T[l];
            for (int i = 0; i < n; i++)
                arr[i] = m[i];
            m = arr;
        }

        public T GetMin()
        {
            T Min = m[0];
            n--;
            m[0] = m[n];
            Heapify(0);
            return Min;
        }

        public T[] GetKMin(int k)
        {
            T[] mas = new T[k];
            for (int i = 0; i < k; i++)
                mas[i] = GetMin();
            return mas;
        }

        private void Heapify(int i)
        {
            int leftChild;
            int rightChild;
            int largestChild;
            for (; ; )
            {
                leftChild = 2 * i + 1;
                rightChild = 2 * i + 2;
                largestChild = i;
                if (leftChild < n && m[leftChild].CompareTo(m[largestChild]) < 0)
                    largestChild = leftChild;
                if (rightChild < n && m[rightChild].CompareTo(m[largestChild]) < 0)
                    largestChild = rightChild;
                if (largestChild == i)
                    break;
                swap(i, largestChild);
                i = largestChild;
            }
        }

        public void Clear()
        {
            n = 0;
        }

        private void swap(int a, int b)
        {
            T t = m[a];
            m[a] = m[b];
            m[b] = t;
        }
    }
}
