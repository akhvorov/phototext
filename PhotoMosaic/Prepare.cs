﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Diagnostics;

namespace PhotoMosaic
{
    public class Prepare
    {
        public string StartDirectory; //имя папки с файлами, откуда брать изображения
        public string DirectoryName; //имя папки с файлами нужного размера

        public Prepare(string Start, string Finish)
        {
            StartDirectory = Start;
            DirectoryName = Finish;
        }
        /// <summary>
        /// Берет фотографии из старой директории и загружает в нужном (квадрат заданного размера) размере в новую директорию
        /// </summary>
        /// <param name="size">размер изображения (сторона квадрата)</param>
        public string[] PreparePhotos(int size)
        {
            string[] FileNames1 = GetNames(StartDirectory); //из начальной папки
            //int len = Math.Min(1800, FileNames1.Length);
            int len = FileNames1.Length;
            string[] FileNames = new string[len];
            for (int i = 0; i < len; i++)
                FileNames[i] = FileNames1[i];
            //Console.WriteLine("Длина массива имен: " + FileNames.Length);
            Bitmap bmp1;
            Bitmap bmp;
            int width, x, y;
            Rectangle rect;
            for (int k = 0; k < FileNames.Length; k += 100)
            {
                for (int i = k; i < FileNames.Length && i < k + 100; i++)
                {
                    try
                    {
                        bmp1 = new Bitmap(StartDirectory + "\\" + FileNames[i]);
                        System.Drawing.Imaging.PixelFormat format = bmp1.PixelFormat;
                        width = Math.Min(bmp1.Size.Height, bmp1.Size.Width);
                        x = (bmp1.Size.Width - width) / 2;
                        y = (bmp1.Size.Height - width) / 2;
                        rect = new Rectangle(x, y, width, width);
                        bmp = bmp1.Clone(rect, format);
                        bmp = new Bitmap(bmp, new Size(size, size));
                        bmp1.Dispose();
                        bmp.Save(DirectoryName + "\\" + FileNames[i]);
                    }
                    catch (System.OutOfMemoryException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
            return FileNames;
        }
        /// <summary>
        /// выдает массив имен файлов без указания пути (фотка.jpg). Только jpg!!!
        /// </summary>
        /// <param name="StartDirectory">имя директории, откуда берутся файлы</param>
        /// <returns>массив имен</returns>
        private static string[] GetNames(string StartDirectory)
        {
            string[] mas;
            string[] jpg = Directory.GetFiles(StartDirectory, "*.jpg"); //из начальной папки
            string s = "";
            int k;
            char ch = '\\';
            for (int i = 0; i < jpg.Length; i++)
            {
                k = jpg[i].Length - 1;
                while (jpg[i][k] != ch)
                {
                    s = jpg[i][k] + s;
                    k--;
                }
                jpg[i] = s;
                s = "";
                //Console.WriteLine("в функции: " + jpg[i]);
            }
            string[] png = Directory.GetFiles(StartDirectory, "*.png"); //из начальной папки
            s = "";
            for (int i = 0; i < png.Length; i++)
            {
                k = png[i].Length - 1;
                while (png[i][k] != ch)
                {
                    s = png[i][k] + s;
                    k--;
                }
                png[i] = s;
                s = "";
                //Console.WriteLine("в функции: " + png[i]);
            }
            mas = new string[png.Length + jpg.Length];
            for (int i = 0; i < jpg.Length; i++)
                mas[i] = jpg[i];
            for (int i = 0; i < png.Length; i++)
                mas[i + jpg.Length] = png[i];
            return mas;
        }
    }
}
