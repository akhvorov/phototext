﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Drawing;

namespace PhotoMosaic
{
    public class Filters
    {
        /// <summary>
        /// наложение второго изображения на первое с процентом прозрачности
        /// </summary>
        /// <param name="main"></param>
        /// <param name="bmp"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static Bitmap Imposition(Bitmap main, Bitmap bmp, int n){
            bmp = new Bitmap(bmp, new Size(main.Size.Width, main.Size.Height));
            int r, g, b;
            Color col1 = new Color();
            Color col2 = new Color();
            Color col = new Color();
            if (n > 100 || n < 0)
            {
                Console.WriteLine("Неправильное значение в процентах!!!");
            }
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col1 = main.GetPixel(width, height);
                    col2 = bmp.GetPixel(width, height);
                    r = (int)(((double)(col1.R*(100 - n) + col2.R*n))/100);
                    g = (int)(((double)(col1.G*(100 - n) + col2.G*n))/100);
                    b = (int)(((double)(col1.B*(100 - n) + col2.B*n))/100);
                    col = Color.FromArgb(r, g, b);
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }
        /// <summary>
        /// делает по краям темнее
        /// </summary>
        /// <param name="main"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static Bitmap Filter1(Bitmap main, int n)
        {
            int x, y, z;
            x = main.Size.Width / 2;
            y = main.Size.Height / 2;
            z = (int)(Math.Pow(x * x + y * y, 0.5) + 1); //расстояние до угла
            int r, g, b, d;
            double coeff;
            Color col1 = new Color();
            Color col = new Color();
            if (n > 100 || n < 0)
            {
                Console.WriteLine("Неправильное значение в процентах!!!");
            }
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col1 = main.GetPixel(width, height);
                    d = (int)Math.Pow(Math.Abs(width - x) * Math.Abs(width - x) + Math.Abs(height - y) * Math.Abs(height - y), 0.5); //расстояние от центра
                    coeff = Math.Pow((((double)Math.Abs(d - z)) / z), (1.2*(((double)n)/100)));
                    /*r = (int)(((double)((100 - n) * col1.R * Math.Abs(d - z))) / (100 * z));
                    g = (int)(((double)((100 - n) * col1.G * Math.Abs(d - z))) / (100 * z));
                    b = (int)(((double)((100 - n) * col1.B * Math.Abs(d - z))) / (100 * z));*/
                    r = (int)(col1.R * (coeff));
                    g = (int)(col1.G * (coeff));
                    b = (int)(col1.B * (coeff));
                    col = Color.FromArgb(r, g, b);
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }
        /// <summary>
        /// затемняет или усветляет изображение (+ затемняет, -усветляет)
        /// </summary>
        /// <param name="main"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static Bitmap Filter2(Bitmap main, int n)
        {
            int r, g, b;
            double coeff;
            Color col1 = new Color();
            Color col = new Color();
            if (n > 100 || n < -100)
            {
                Console.WriteLine("Неправильное значение в процентах!!!");
            }
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col1 = main.GetPixel(width, height);
                    coeff = (double)(100 - n) / 100;
                    r = Math.Min(255, (int)(col1.R * coeff));
                    g = Math.Min(255, (int)(col1.G * coeff));
                    b = Math.Min(255, (int)(col1.B * coeff));
                    col = Color.FromArgb(r, g, b);
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }
        /// <summary>
        /// не знаю, что он делает, но получилось клево
        /// </summary>
        /// <param name="main"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static Bitmap Filter3(Bitmap main, int n)
        {
            int r, g, b;
            double coeff;
            Color col1 = new Color();
            Color col = new Color();
            if (n > 100 || n < 0)
            {
                Console.WriteLine("Неправильное значение в процентах!!!");
            }
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col1 = main.GetPixel(width, height);
                    coeff = ((double)(100 - n) / 100);
                    //изменяет цвет в обратную сторону и умножает на не тот коэффициент
                    r = Math.Min(255, (int)(col1.R * (1 - ((double)(col1.R - 128) / 128)) * coeff));
                    g = Math.Min(255, (int)(col1.G * (1 - ((double)(col1.G - 128) / 128)) * coeff));
                    b = Math.Min(255, (int)(col1.B * (1 - ((double)(col1.B - 128) / 128)) * coeff));
                    col = Color.FromArgb(r, g, b);
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }
        /// <summary>
        /// контраст
        /// </summary>
        /// <param name="main"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static Bitmap Filter4(Bitmap main, int n)
        {
            int r, g, b;
            double coeff;
            Color col1 = new Color();
            Color col = new Color();
            if (n > 100 || n < 0)
            {
                Console.WriteLine("Неправильное значение в процентах!!!");
            }
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col1 = main.GetPixel(width, height);
                    coeff = ((double)(n) / 100);
                    r = Math.Min(255, (int)(col1.R * (1 + (((double)(col1.R - 128) / 128) * coeff))));
                    g = Math.Min(255, (int)(col1.G * (1 + (((double)(col1.G - 128) / 128) * coeff))));
                    b = Math.Min(255, (int)(col1.B * (1 + (((double)(col1.B - 128) / 128) * coeff))));
                    col = Color.FromArgb(r, g, b);
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }
        /// <summary>
        /// негатив
        /// </summary>
        /// <param name="main"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static Bitmap Filter5(Bitmap main)
        {
            int r, g, b;
            Color col1 = new Color();
            Color col = new Color();
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col1 = main.GetPixel(width, height);
                    r = Math.Min(255, (int)(col1.R + (-2) * (col1.R - 128)));
                    g = Math.Min(255, (int)(col1.G + (-2) * (col1.G - 128)));
                    b = Math.Min(255, (int)(col1.B + (-2) * (col1.B - 128)));
                    col = Color.FromArgb(r, g, b);
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }
        /// <summary>
        /// заменяет все пиксели на рандомные близкие к тому, что было. Нечеткость зависит от выбранного параметра
        /// </summary>
        /// <param name="main"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static Bitmap Filter6(Bitmap main, int n)
        {
            int r, g, b;
            double coeff;
            Random ran = new Random();
            Color col1 = new Color();
            Color col = new Color();
            if (n > 100 || n < 0)
            {
                Console.WriteLine("Неправильное значение в процентах!!!");
            }
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col1 = main.GetPixel(width, height);
                    coeff = ((double)(n) / 100) * (((double)ran.Next(300)/100) - 1.5);
                    r = Math.Min(255, Math.Max(0, (int)(col1.R * (1 + coeff))));
                    g = Math.Min(255, Math.Max(0, (int)(col1.G * (1 + coeff))));
                    b = Math.Min(255, Math.Max(0, (int)(col1.B * (1 + coeff))));
                    col = Color.FromArgb(r, g, b);
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }
        /// <summary>
        /// Размывает изображение
        /// </summary>
        /// <param name="main"></param>
        /// <param name="n">3 или 5 (как сильно размыть)</param>
        /// <returns></returns>
        public static Bitmap Filter7(Bitmap main, int n)
        {
            int[,] matrix = new int[1,1];
            matrix[0, 0] = 1;
            int len = 1, div = 1;
            switch (n)
            {
                case 3: matrix = new int[3, 3];
                    /*matrix[0, 0] = 2;
                    matrix[0, 1] = 3;
                    matrix[0, 2] = 2;
                    matrix[1, 0] = 3;
                    matrix[1, 1] = 5;
                    matrix[1, 2] = 3;
                    matrix[2, 0] = 2;
                    matrix[2, 1] = 3;
                    matrix[2, 2] = 2;
                    len = 3;
                    div = 25;*/
                    matrix[0, 0] = 1;
                    matrix[0, 1] = 2;
                    matrix[0, 2] = 1;
                    matrix[1, 0] = 2;
                    matrix[1, 1] = 4;
                    matrix[1, 2] = 2;
                    matrix[2, 0] = 1;
                    matrix[2, 1] = 2;
                    matrix[2, 2] = 1;
                    len = 3;
                    div = 16;
                    break;
                case 5:
                    matrix = new int[5, 5];
                    matrix[0, 0] = 1; matrix[0, 1] = 2; matrix[0, 2] = 3; matrix[0, 3] = 2; matrix[0, 4] = 1;
                    matrix[1, 0] = 2; matrix[1, 1] = 4; matrix[1, 2] = 6; matrix[1, 3] = 4; matrix[1, 4] = 2;
                    matrix[2, 0] = 3; matrix[2, 1] = 6; matrix[2, 2] = 10; matrix[2, 3] = 6; matrix[2, 4] = 3;
                    matrix[3, 0] = 2; matrix[3, 1] = 4; matrix[3, 2] = 6; matrix[3, 3] = 4; matrix[3, 4] = 2;
                    matrix[4, 0] = 1; matrix[4, 1] = 2; matrix[4, 2] = 3; matrix[4, 3] = 2; matrix[4, 4] = 1;
                    len = 5;
                    div = 82;
                    break;
                //case 10:
            }
            double r, g, b;
            Color col = new Color();
            Bitmap bmp = new Bitmap(main.Size.Width + len, main.Size.Height + len);
            for (int i = 0; i < main.Size.Width; i++)
            {
                for (int j = 0; j < main.Size.Height; j++)
                {
                    bmp.SetPixel(i + (len / 2), j + (len / 2), main.GetPixel(i, j));
                }
            }
            for (int width = (len / 2); width < main.Size.Width + (len / 2); width++)
            {
                for (int height = (len / 2); height < main.Size.Height + (len / 2); height++)
                {
                    r = 0;
                    g = 0;
                    b = 0;
                    for (int i = 0; i < len; i++)
                    {
                        for (int j = 0; j < len; j++)
                        {
                            col = bmp.GetPixel(width + i - (len / 2), height + j - (len / 2));
                            r += matrix[i, j] * col.R;
                            g += matrix[i, j] * col.G;
                            b += matrix[i, j] * col.B;
                        }
                    }
                    r = r / div;
                    g = g / div;
                    b = b / div;
                    col = Color.FromArgb((int)r, (int)g, (int)b);
                    bmp.SetPixel(width, height, col);
                }
            }
            for (int i = 0; i < main.Size.Width; i++)
            {
                for (int j = 0; j < main.Size.Height; j++)
                {
                    main.SetPixel(i, j, bmp.GetPixel(i + (len / 2), j + (len / 2)));
                }
            }

            return main;
        }
        /// <summary>
        /// делает изображение четче, но тут что-то не так еще(
        /// </summary>
        /// <param name="main"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static Bitmap Filter8(Bitmap main, int n)
        {
            int[,] matrix = new int[1, 1];
            matrix[0, 0] = 1;
            int len = 1, div = 1;
            switch (n)
            {
                case 3: matrix = new int[3, 3];
                    matrix[0, 0] = -1;
                    matrix[0, 1] = -1;
                    matrix[0, 2] = -1;
                    matrix[1, 0] = -1;
                    matrix[1, 1] = 9;
                    matrix[1, 2] = -1;
                    matrix[2, 0] = -1;
                    matrix[2, 1] = -1;
                    matrix[2, 2] = -1;
                    len = 3;
                    div = 1;
                    break;
                case 5:
                    matrix = new int[5, 5];
                    matrix[0, 0] = 1; matrix[0, 1] = 2; matrix[0, 2] = 3; matrix[0, 3] = 2; matrix[0, 4] = 1;
                    matrix[1, 0] = 2; matrix[1, 1] = 4; matrix[1, 2] = 6; matrix[1, 3] = 4; matrix[1, 4] = 2;
                    matrix[2, 0] = 3; matrix[2, 1] = 6; matrix[2, 2] = 10; matrix[2, 3] = 6; matrix[2, 4] = 3;
                    matrix[3, 0] = 2; matrix[3, 1] = 4; matrix[3, 2] = 6; matrix[3, 3] = 4; matrix[3, 4] = 2;
                    matrix[4, 0] = 1; matrix[4, 1] = 2; matrix[4, 2] = 3; matrix[4, 3] = 2; matrix[4, 4] = 1;
                    len = 5;
                    div = 82;
                    break;
                //case 10:
            }
            Console.WriteLine(len/2);
            double r, g, b;
            Color col = new Color();
            Bitmap bmp = new Bitmap(main.Size.Width + len - 1, main.Size.Height + len - 1);
            for (int i = 0; i < main.Size.Width; i++)
            {
                for (int j = 0; j < main.Size.Height; j++)
                {
                    bmp.SetPixel(i + (len / 2), j + (len / 2), main.GetPixel(i, j));
                }
            }
            for (int width = (len / 2); width < main.Size.Width + (len / 2); width++)
            {
                for (int height = (len / 2); height < main.Size.Height + (len / 2); height++)
                {
                    r = 0;
                    g = 0;
                    b = 0;
                    for (int i = 0; i < len; i++)
                    {
                        for (int j = 0; j < len; j++)
                        {
                            col = bmp.GetPixel(width + i - (len / 2), height + j - (len / 2));
                            r += matrix[i, j] * col.R;
                            g += matrix[i, j] * col.G;
                            b += matrix[i, j] * col.B;
                        }
                    }
                    r = Math.Min(255, Math.Max(0, r / div));
                    g = Math.Min(255, Math.Max(0, g / div));
                    b = Math.Min(255, Math.Max(0, b / div));
                    col = Color.FromArgb((int)r, (int)g, (int)b);
                    main.SetPixel(width - (len/2), height - (len/2), col);
                }
            }

            return main;
        }
        /// <summary>
        /// Сглаживает изображение (Median)
        /// </summary>
        /// <param name="main"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static Bitmap Filter9(Bitmap main, int n)
        {
            //double r, g, b;
            List<int> r = new List<int>();
            List<int> g = new List<int>();
            List<int> b = new List<int>();
            Color col = new Color();
            Bitmap bmp = new Bitmap(main.Size.Width + n, main.Size.Height + n);
            for (int i = 0; i < main.Size.Width; i++)
            {
                for (int j = 0; j < main.Size.Height; j++)
                {
                    bmp.SetPixel(i + (n / 2), j + (n / 2), main.GetPixel(i, j));
                }
            }
            for (int width = (n / 2); width < main.Size.Width + (n / 2); width++)
            {
                for (int height = (n / 2); height < main.Size.Height + (n / 2); height++)
                {
                    r.Clear();
                    g.Clear();
                    b.Clear();
                    for (int i = 0; i < n; i++)
                    {
                        for (int j = 0; j < n; j++)
                        {
                            col = bmp.GetPixel(width + i - (n / 2), height + j - (n / 2));
                            r.Add(col.R);
                            g.Add(col.G);
                            b.Add(col.B);
                        }
                    }
                    r = Sort(r);
                    g = Sort(g);
                    b = Sort(b);
                    col = Color.FromArgb(r[n / 2], g[n / 2], b[n / 2]);
                    //col = Color.FromArgb(r[n - 1], g[n - 1], b[n - 1]);
                    //col = Color.FromArgb(r[0], g[0], b[0]);
                    main.SetPixel(width - (n / 2), height - (n / 2), col);
                }
            }

            return main;
        }

        /// <summary>
        /// некоторые пиксели меняет на случайный
        /// </summary>
        /// <param name="main"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static Bitmap Filter10(Bitmap main, int n)
        {
            int r, g, b;
            Color col = new Color();
            int k = 0;
            Random rand = new Random();
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    k = rand.Next(n);
                    if (k == 0)
                    {
                        r = rand.Next(255);
                        g = rand.Next(255);
                        b = rand.Next(255);
                        col = Color.FromArgb(r, g, b);
                        main.SetPixel(width, height, col);
                    }
                }
            }
            return main;
        }

        /// <summary>
        /// делает черно-белым
        /// </summary>
        /// <param name="main"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static Bitmap Filter11(Bitmap main)
        {
            Color col = new Color();
            int k = 0;
            double[] coeff = new double[3];
            /*coeff[0] = 0.333;
            coeff[1] = 0.333;
            coeff[2] = 0.333;*/
            /*coeff[0] = 0.299;
            coeff[1] = 0.587;
            coeff[2] = 0.114;*/
            coeff[0] = 0.2126;
            coeff[1] = 0.7152;
            coeff[2] = 0.0722;
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    k = Math.Min(255, (int)(coeff[0] * (double)col.R + coeff[1] * (double)col.G + coeff[2] * (double)col.B));
                    col = Color.FromArgb(k, k, k);
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }

        /// <summary>
        /// делает краснее
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static Bitmap Filter12(Bitmap main, int n)
        {
            int r, g, b;
            double coeff;
            Color col1 = new Color();
            Color col = new Color();
            if (n > 100 || n < -100)
            {
                Console.WriteLine("Неправильное значение в процентах!!!");
            }
            coeff = (double)(100 + n) / 100;
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col1 = main.GetPixel(width, height);
                    //if (col1.R + col1.G + col1.B > 150)
                    //{
                        r = Math.Min(255, (int)((col1.R) * coeff));
                        g = Math.Min(255, (int)((col1.G) / coeff));
                        b = Math.Min(255, (int)((col1.B) / coeff));
                        col = Color.FromArgb(r, g, b);
                        main.SetPixel(width, height, col);
                    //}
                }
            }
            return main;
        }

        /// <summary>
        /// делает бинарное черное-белое изображение
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static Bitmap Filter13(Bitmap main)
        {
            //TODO:разбивать изображение на части и находить средний для каждой из частей
            //сделать фильтр не бинарный, а самому задавать число цветов, столько и делать
            double coeff = 0.8;
            main = Filter11(main);
            int av = 0;
            Color col = new Color();
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    av += col.B; //тут неважно, какой цвет
                }
            }
            av = (av / (main.Size.Width * main.Size.Height));
            int k = 0;
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    if (col.B >= av*coeff)
                    {
                        k = 255;
                    }
                    else
                    {
                        k = 0;
                    }
                    col = Color.FromArgb(k, k, k);
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }

        /// <summary>
        /// делает бинарное чернобелое но большими пикселями
        /// </summary>
        /// <param name="main"></param>
        /// <param name="n">длина квадрата</param>
        /// <returns></returns>
        public static Bitmap Filter14(Bitmap main, int n)
        {
            //TODO:разбивать изображение на части и находить средний для каждой из частей
            //сделать фильтр не бинарный, а самому задавать число цветов, столько и делать
            double coeff = 1; //больше - темнее, меньше - светлее
            main = Filter11(main);
            int av = 0;
            Color col = new Color();
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    av += col.B; //тут неважно, какой цвет
                }
            }
            av = (av / (main.Size.Width * main.Size.Height));
            int k = 0;
            int aver2 = 0;
            main = new Bitmap(main, new Size(((main.Size.Width + (n / 2)) / n) * n, ((main.Size.Height + (n / 2)) / n) * n));
            for (int width = 0; width < main.Size.Width; width += n)
            {
                for (int height = 0; height < main.Size.Height; height += n)
                {
                    for (int x = 0; x < n; x++)
                    {
                        for (int y = 0; y < n; y++)
                        {
                            col = main.GetPixel(width + x, height + y);
                            aver2 += col.B;
                        }
                    }
                    aver2 = aver2/(n*n);
                    if (aver2 >= av * coeff)
                    {
                        k = 255;
                    }
                    else
                    {
                        k = 0;
                    }
                    for (int x = 0; x < n; x++)
                    {
                        for (int y = 0; y < n; y++)
                        {
                            col = Color.FromArgb(k, k, k);
                            main.SetPixel(width + x, height + y, col);
                        }
                    }
                }
            }
            return main;
        }
        /// <summary>
        /// черно белое с заданным кол-вом цветов
        /// </summary>
        /// <param name="main"></param>
        /// <param name="n">кол-во цветов</param>
        /// <param name="bitMask">битовая маска цветов</param>
        /// <returns></returns>
        public static Bitmap Filter15(Bitmap main, int n, int bitMask = 7)
        {
            int step = 255/n;
            main = Filter11(main);
            Color col = new Color();
            int k = 0;
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    /*if (col.B >= 0 && col.B < step / 2)
                    {
                        k = 0;
                    }*/
                    for (int i = 0; i < n; i++)
                    {
                        if (col.B >= i * step && col.B < (i + 1) * step)
                        {
                            //k = Math.Min(Math.Max(i * step, 0), 255);
                            k = i * step;
                            break;
                        }
                    }
                    if (col.B >= (n-1)*step && col.B <= 255)
                    {
                        k = 255;
                    }
                    col = Color.FromArgb(k * ((bitMask & 4) >> 2), k * ((bitMask & 2) >> 1), k * (bitMask & 1));
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }
        /// <summary>
        /// рандомную фигню делает, связанную со смещением
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static Bitmap Filter16(Bitmap main)
        {
            int r, g, b;
            Random ran = new Random();
            int offset = ran.Next(255);
            Color col = new Color();
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    r = (col.R + ((ran.Next(1) * 2 - 1) * offset) + 255) % 255;
                    g = (col.G + ((ran.Next(1) * 2 - 1) * offset) + 255) % 255;
                    b = (col.B + ((ran.Next(1) * 2 - 1) * offset) + 255) % 255;
                    col = Color.FromArgb(r, g, b);
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }
        /// <summary>
        /// делает сдвиг
        /// </summary>
        /// <param name="main"></param>
        /// <param name="n">на сколько сдвиг</param>
        /// <param name="bitMask">десятичная(!) битовая маска</param>
        /// <returns></returns>
        public static Bitmap Filter17(Bitmap main, int n, int bitMask = 111)
        {
            int y = -1;
            int r, g, b;
            Color col = new Color();
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    r = (col.R + y*n*(bitMask / 100) + 255) % 255;
                    g = (col.G + y*n*((bitMask / 10) % 10) + 255) % 255;
                    b = (col.B + y*n*(bitMask % 10) + 255) % 255;
                    col = Color.FromArgb(r, g, b);
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }

        /// <summary>
        /// делает бинарное черное-белое изображение улучшенное
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static Bitmap Filter18(Bitmap main)
        {
            //TODO:разбивать изображение на части и находить средний для каждой из частей
            //сделать фильтр не бинарный, а самому задавать число цветов, столько и делать
            double coeff = 0.8;
            main = Filter11(main);
            int[][] mas = new int[main.Size.Width][];
            for (int i = 0; i < main.Size.Width; i++)
            {
                mas[i] = new int[main.Size.Height];
                for (int j = 0; j < main.Size.Height; j++)
                {
                    mas[i][j] = 0;
                }
            }
            int[][] counts = new int[main.Size.Width][];
            for (int i = 0; i < main.Size.Width; i++)
            {
                counts[i] = new int[main.Size.Height];
                for (int j = 0; j < main.Size.Height; j++)
                {
                    counts[i][j] = 0;
                }
            }
            int size = Math.Min(main.Size.Height, main.Size.Width);
            int av = 0;
            Color col = new Color();
            int count = 0;
            while (size > 5)
            {
                for (int width = 0; width < main.Size.Width; width += (size))
                {
                    for (int height = 0; height < main.Size.Height; height += (size))
                    {
                        for (int x = 0; (x < size) && (width + x < main.Size.Width); x++)
                        {
                            for (int y = 0; (y < size) && (height + y < main.Size.Height); y++)
                            {
                                col = main.GetPixel(width + x, height + y);
                                av += col.B;
                                mas[width + x][height + y] = (mas[width + x][height + y] * counts[width + x][height + y] + col.B) / (counts[width + x][height + y] + 1);
                                //counts[width + x][height + y]++;
                            }
                        }
                        av = av / (size * size);
                        for (int x = 0; (x < size) && (width + x < main.Size.Width); x++)
                        {
                            for (int y = 0; (y < size) && (height + y < main.Size.Height); y++)
                            {
                                mas[width + x][height + y] = (mas[width + x][height + y] * counts[width + x][height + y] + av) / (counts[width + x][height + y] + 1);
                                counts[width + x][height + y]++;
                            }
                        }
                        /*col = main.GetPixel(width, height);
                        av += col.B; //тут неважно, какой цвет*/
                    }
                }
                count++;
                size = (int)((double)size / 1.5);
            }
            int k = 0;
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    if (col.B >= mas[width][height])
                    {
                        k = 255;
                    }
                    else
                    {
                        k = 0;
                    }
                    col = Color.FromArgb(k, k, k);
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }
        /// <summary>
        /// делает чб и растягивает диапозон на [0; 255]
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static Bitmap Filter19(Bitmap main){
            main = Filter11(main);
            int min = 255, max = 0;
            Color col = new Color();
            int k = 0;
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    min = Math.Min(min, col.B);
                    max = Math.Max(max, col.B);
                }
            }
            double step = 255.0 / (double)(max - min);
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    k = (int)((col.B - min) * step);
                    col = Color.FromArgb(k, k, k);
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }
        /// <summary>
        /// растягивает диапозон на [0; 255]
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static Bitmap Filter20(Bitmap main)
        {
            int minR = 255, maxR = 0;
            int minG = 255, maxG = 0;
            int minB = 255, maxB = 0;
            Color col = new Color();
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    minR = Math.Min(minR, col.R);
                    maxR = Math.Max(maxR, col.R);
                    minG = Math.Min(minG, col.G);
                    maxG = Math.Max(maxG, col.G);
                    minB = Math.Min(minB, col.B);
                    maxB = Math.Max(maxB, col.B);
                }
            }
            double stepR = 255.0 / (double)(maxR - minR);
            double stepG = 255.0 / (double)(maxG - minG);
            double stepB = 255.0 / (double)(maxB - minB);
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    col = Color.FromArgb((int)((col.R - minR) * stepR), (int)((col.G - minG) * stepG), (int)((col.B - minB) * stepB));
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }
        /// <summary>
        /// Эквализация чб
        /// </summary>
        /// <param name="main"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public static Bitmap Filter21(Bitmap main, int k = 255)
        {
            main = Filter11(main);
            //Bitmap pic = new Bitmap(main);
            double[] arr = new double[k + 1];
            Color col;
            for (int i = 0; i <= k; i++)
                arr[i] = 0;
            for (int width = 0; width < main.Size.Width; width++)
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    arr[col.R * k / 255]++;
                }
            for (int i = 0; i <= k; i++)
                arr[i] = arr[i] / (double)(main.Size.Height * main.Size.Width);
            for (int i = 1; i <= k; i++)
                arr[i] += arr[i - 1];
            int n;
            for (int width = 0; width < main.Size.Width; width++)
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    n = (int)(arr[col.R * k / 255] * 255);
                    main.SetPixel(width, height, Color.FromArgb(n, n, n));
                }
            return main;
        }
        /// <summary>
        /// Эквализация цветного
        /// </summary>
        /// <param name="main"></param>
        /// <param name="k">от нуля до какого</param>
        /// <returns></returns>
        public static Bitmap Filter22(Bitmap main, int k = 255)
        {
            double[] r = new double[k + 1];
            double[] g = new double[k + 1];
            double[] b = new double[k + 1];
            Color col;
            for (int i = 0; i <= k; i++)
            {
                r[i] = 0;
                g[i] = 0;
                b[i] = 0;
            }
            for (int width = 0; width < main.Size.Width; width++)
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    r[col.R * k / 255]++;
                    g[col.G * k / 255]++;
                    b[col.B * k / 255]++;
                }
            for (int i = 0; i <= k; i++)
            {
                r[i] /= (double)(main.Size.Height * main.Size.Width);
                g[i] /= (double)(main.Size.Height * main.Size.Width);
                b[i] /= (double)(main.Size.Height * main.Size.Width);
            }
            for (int i = 1; i <= k; i++)
            {
                r[i] += r[i - 1];
                g[i] += g[i - 1];
                b[i] += b[i - 1];
            }
            for (int width = 0; width < main.Size.Width; width++)
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    main.SetPixel(width, height, Color.FromArgb((int)(r[col.R * k / 255] * 255), (int)(g[col.G * k / 255] * 255), (int)(b[col.B * k / 255] * 255)));
                }
            return main;
        }
        /// <summary>
        /// делает трехцветное (красный, серый, белый)
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static Bitmap Filter23(Bitmap main)
        {
            main = Filter7(Filter7(main, 3), 3);
            int n = 4;
            Color[] mas = new Color[n];
            mas[3] = Color.FromArgb(251, 227, 165);
            mas[2] = Color.FromArgb(106, 145, 162);
            mas[1] = Color.FromArgb(216, 37, 41);
            mas[0] = Color.FromArgb(8, 53, 75);
            int step = 255 / n;
            main = Filter19(Filter11(main));
            Color col = new Color();
            int k = 0;
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    for (int i = 0; i < n; i++)
                        if (col.B >= i * step && col.B < (i + 1) * step)
                        {
                            k = i;
                            break;
                        }
                    if (col.B >= (n - 1) * step && col.B <= 255)
                        k = 3;
                    col = mas[k];
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }
        /// <summary>
        /// делает битовый сдвиг пикселю
        /// </summary>
        /// <param name="main"></param>
        /// <returns></returns>
        public static Bitmap Filter24(Bitmap main, int n)
        {
            int r, g, b;
            Color col = new Color();
            for (int width = 0; width < main.Size.Width; width++)
            {
                for (int height = 0; height < main.Size.Height; height++)
                {
                    col = main.GetPixel(width, height);
                    r = (col.R << n) % 256;
                    g = (col.G << n) % 256;
                    b = (col.B << n) % 256;
                    col = Color.FromArgb(r, g, b);
                    main.SetPixel(width, height, col);
                }
            }
            return main;
        }


        public static List<int> Sort(List<int> l)
        {
            int t;
            for (int i = l.Count - 1; i >= 0; i--)
            {
                for (int k = 0; k < i; k++)
                {
                    if (l[k] > l[k + 1])
                    {
                        t = l[k];
                        l[k] = l[k + 1];
                        l[k + 1] = t;
                    }
                }
            }
            return l;
        }

        public static Bitmap IndexateImages(string directory)
        {
            string[] names = Directory.GetFiles(directory);
            Color[] colmas = new Color[256];
            int[] red = new int[256], gr = new int[256], bl = new int[256];
            for (int i = 0; i < 256; i++)
            {
                red[i] = 0;
                gr[i] = 0;
                bl[i] = 0;
            }
            for (int count = 0; count < 300/*names.Length - 100*/; count += 50)
            {
                List<int>[] table = GetArrayList(names, count, count + 100);
                Console.WriteLine(count + "First part is ended!");
                long r = 0, g = 0, b = 0;
                for (int i = 0; i < 256; i++)
                {
                    if (table[i].Count == 0)
                    {
                        //colmas[i] = Color.FromArgb(255, 255, 255);
                        Console.WriteLine(i + " -st color is not exist");
                        continue;
                    }
                    for (int j = 0; j < table[i].Count; j++)
                    {
                        r += table[i][j] / 1000000;
                        g += table[i][j] / 1000 % 1000;
                        b += table[i][j] % 1000;
                    }
                    r /= table[i].Count;
                    g /= table[i].Count;
                    b /= table[i].Count;
                    red[i] += (int)Math.Min(255, r);
                    gr[i] += (int)Math.Min(255, g);
                    bl[i] += (int)Math.Min(255, b);
                    //colmas[i] = Color.FromArgb((int)r, (int)g, (int)b);
                }
                r = 0; g = 0; b = 0;
            }
            for (int i = 0; i < 256; i++)
                colmas[i] = Color.FromArgb((red[i] / (6)), (gr[i] / (6)), (bl[i] / (6)));
            Bitmap pic = new Bitmap(256 * 4, 200);
            for (int widht = 0; widht < pic.Width; widht++)
                for (int height = 0; height < pic.Height; height++)
                    pic.SetPixel(widht, height, colmas[widht / 4]);
            return pic;
            /*
            int MAX = 5;Dictionary<Color, int> map = new Dictionary<Color, int>();
            List<Color>[] bestCols = new List<Color>[256];
            int r = 0, g = 0, b = 0;
            for (int i = 0; i < 256; i++)
            {
                for (int j = 0; j < table[i].Count; j++)
                {
                    if (!map.ContainsKey(table[i][j]))
                        map.Add(table[i][j], 1);
                    else
                        map[table[i][j]]++;
                }
                for()
            }
            Console.WriteLine("Second part is ended");*/

        }

        public static List<int>[] GetArrayList(string[] names, int st, int fi)
        {
            
            List<int>[] table = new List<int>[256];
            for (int i = 0; i < 256; i++)
            {
                table[i] = new List<int>();
            }
            Color col;
            double[] coeff = new double[3];
            coeff[0] = 0.2126;
            coeff[1] = 0.7152;
            coeff[2] = 0.0722;
            Bitmap pic;
            for (int i = st; i < fi; i++)
            {
                try
                {
                    pic = new Bitmap(names[i]);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    continue;
                }
                for (int j = 0; j < pic.Width; j++)
                {
                    for (int k = 0; k < pic.Height; k++)
                    {
                        col = pic.GetPixel(j, k);
                        table[Math.Min(255, (int)(coeff[0] * (double)col.R + coeff[1] * (double)col.G + coeff[2] * (double)col.B))].Add(col.R*1000000 + col.G*1000 + col.B);
                    }
                }
                pic.Dispose();
            }
            return table;
        }
    }
}
