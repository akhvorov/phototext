﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Diagnostics;

namespace PhotoMosaic
{
    public class Mosaic
    {
        public string DirectoryName;
        public Bitmap pic; //изображение, над которым будем работать
        public string[] FileNames; //массив имен файлов в папке
        public List<Color> colors;
        public int size;

        public Mosaic(int s, Bitmap original)
        {
            size = s;
            colors = new List<Color>();
            pic = new Bitmap(original, new Size(((original.Size.Width + (size / 2)) / size) * size, ((original.Size.Height + (size / 2)) / size) * size));
            original.Dispose();
        }
        /// <summary>
        /// готовит фотографии, приводя в нужный размер и директорию
        /// </summary>
        /// <param name="StartDirectory">откуда выкачали оригиналы</param>
        /// <param name="DirectoryName">куда вкачали обработанные</param>
        public void Prepare(string StartDirectory, string DirectoryName)
        {
            this.DirectoryName = DirectoryName;
            Prepare pr = new Prepare(StartDirectory, DirectoryName);
            FileNames = pr.PreparePhotos(size);
        }
        /// <summary>
        /// заполняет список цветов
        /// </summary>
        public void TakeColors()
        {
            colors = AverageColor.GetColors(DirectoryName, FileNames);
        }

        public Bitmap MakeMosaic(/*Bitmap original*/)
        {
            /*pic = new Bitmap(original, new Size(((original.Size.Width + (size / 2)) / size) * size, ((original.Size.Height + (size / 2)) / size) * size));
            original.Dispose();*/
            Bitmap bmp;
            Rectangle rect;
            System.Drawing.Imaging.PixelFormat format = pic.PixelFormat;
            for (int height = 0; height < pic.Size.Height; height += size)
            {
                for (int width = 0; width < pic.Size.Width; width += size)
                {
                    rect = new Rectangle(width, height, size, size);
                    bmp = pic.Clone(rect, format);
                    bmp = new Bitmap(DirectoryName + "\\" + FileNames[AverageColor.FindSamePic(colors, bmp)]);
                    for (int i = 0; i < size; i++)
                        for (int j = 0; j < size; j++)
                            pic.SetPixel(width + j, height + i, bmp.GetPixel(j, i));
                }
            }
            return pic;
        }
    }
}
