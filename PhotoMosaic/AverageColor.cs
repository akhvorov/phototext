﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace PhotoMosaic
{
    class AverageColor
    {
        public string DirectoryName;
        public string[] FileNames;

        public AverageColor(string DirectoryName, string[] FileNames)
        {
            this.DirectoryName = DirectoryName;
            this.FileNames = FileNames;
        }
        /// <summary>
        /// вычислет средний цвет картинки
        /// </summary>
        /// <param name="bmp">картинка</param>
        /// <param name="size">размер</param>
        /// <returns>средний цвет</returns>
        public static Color GetAverageColor(Bitmap bmp)
        {
            int size = bmp.Size.Height;
            Color col = new Color();
            int a = 0;
            int r = 0;
            int g = 0;
            int b = 0;
            for (int height = 0; height < size; height++)
            {
                for (int widht = 0; widht < size; widht++)
                {
                    col = bmp.GetPixel(height, widht);
                    a += col.A;
                    r += col.R;
                    g += col.G;
                    b += col.B;
                }
            }
            col = Color.FromArgb((a) / (size * size), (r) / (size * size), (g) / (size * size), (b) / (size * size));
            return col;
        }
        /// <summary>
        /// вычисляет средний цвет каждой картинки в директории и возвращает список с совпадающими индексами
        /// </summary>
        /// <param name="DirectoryName">имя директории</param>
        /// <param name="FileNames">имена файлов</param>
        /// <returns>список средних цветов</returns>
        public static List<Color> GetColors(string DirectoryName, string[] FileNames)
        {
            List<Color> col = new List<Color>();
            Bitmap bmp;
            for (int i = 0; i < FileNames.Length; i++)
            {
                bmp = new Bitmap(DirectoryName + "\\" + FileNames[i]);
                col.Add(GetAverageColor(bmp));
            }
            return col;
        }
        /// <summary>
        /// находит номер наиболее подходящей картинки для замены
        /// </summary>
        /// <param name="colors">список цветов</param>
        /// <param name="bmp">картинка, замена которой нам нужна</param>
        /// <returns>индекс картинки</returns>
        public static int FindSamePic(List<Color> colors, Bitmap bmp)
        {
            Color col = GetAverageColor(bmp);
            int[] coeffs = new int[colors.Count];
            int a = 0, b = 0, r = 0, g = 0; //на а пока забиваем
            for (int i = 0; i < colors.Count; i++)
            {
                b = (colors[i].B - col.B) * (colors[i].B - col.B);
                g = (colors[i].G - col.G) * (colors[i].G - col.G);
                r = (colors[i].R - col.R) * (colors[i].R - col.R);
                coeffs[i] = 30 * r + 59 * g + 11 * b; //можно как-то иначе считать коеффициент
            }
            int index = 0;
            int t = coeffs[0];
            for (int i = 0; i < coeffs.Length; i++)
            {
                if (coeffs[i] < t)
                {
                    t = coeffs[i];
                    index = i;
                }
            }
            List<int> inds = new List<int>();
            for (int i = 0; i < coeffs.Length; i++)
                if (Math.Abs(coeffs[i] - t) < 45*45*10) //45 (45*45*100) - должно быть
                    inds.Add(i);
            Random ran = new Random();
            return inds[ran.Next(inds.Count)];
        }
    }
}
